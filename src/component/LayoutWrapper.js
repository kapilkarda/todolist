import React, { Component } from 'react'

import Sidebar from './Sidebar/Sidebar';

export default class LayoutWrapper extends Component {
    render() {
        return (
            <div>                
                <Sidebar />
            </div>
            
        )
    }
}
