import React, { Component } from 'react';
import iconImg from '../../assets/images/Capture.JPG';

export default class Sidebar extends Component {
    render() {
        return (
            <div className="icon-bar sidebar">
                <div className="cal_height">
                <img src={iconImg}  alt="icon" className="img_wdt"/>
                <span className="active"><i className="fa fa-check bdr"></i></span> 
                <span><i className="fa fa-users"></i></span>
                <span><i className="fa fa-pie-chart"></i></span>
                </div>
                <div className="setting_height">
                    <div className="height75">
                        <span><i className="fa fa-question-circle"></i></span>
                    </div>
                    <div className="height75">
                        <span><i className="fa fa-cog"></i></span>
                    </div>
                </div>
            </div>
        )
    }
}
