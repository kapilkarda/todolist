import React, { Component } from 'react'

export default class Header extends Component {
    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                        <div className="col-md-3"></div>
                        <div className="col-md-7 header_bg">
                            <div className="icon-headerbar">
                                <span className="active-header"><i className="fa fa-th-list"></i></span> 
                                <span className="header-bar"><i className="fa fa-calendar-o"></i></span> 
                                <span className="header-bar"><i className="fa fa-star"></i></span>
                                <div className="flot_right">
                                    <span><i className="fa fa-search"></i></span>
                                </div>
                                <div className="flot_right">
                                    <span><i className="fa fa-sliders"></i></span>
                                </div>
                                <div className="flot_right">
                                    <span className="header_bdr"></span>
                                </div>
                                <div className="flot_right">
                                    <span>Archive</span>
                                </div>
                                <div className="flot_right active-header-btm">
                                    <span className="fnt_clr_active">Tasks</span>
                                </div>                            
                            </div>                            
                        </div>
                        <div className="col-md-2">
                        </div>
                    </div>                    
                </div>
            </div>            
        )
    }
}
