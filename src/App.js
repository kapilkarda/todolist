import React from 'react';
import './App.css';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Task from './container/Task/Task';

class App extends React.PureComponent{
  render(){
    return (
      <React.Fragment>
        <BrowserRouter>
					<Switch>
            <Route exact={true} path="/" component={Task} />
          </Switch>
        </BrowserRouter>
      </React.Fragment>
    );
  }
  
}

export default App;
