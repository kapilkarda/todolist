import React, {useState} from 'react'
import LayoutWrapper from '../../component/LayoutWrapper';
import Header from '../../component/Header/Header';
import './Task.css'
import Modal from "react-responsive-modal";
import 'react-responsive-modal/styles.css';
import {DragDropContext, Droppable, Draggable} from 'react-beautiful-dnd'

// a little function to help us with reordering the result
const reorder = (list, startIndex, endIndex) => {
  const result = Array.from(list);
  const [removed] = result.splice(startIndex, 1);
  result.splice(endIndex, 0, removed);

  return result;
};

const grid = 8;

const getItemStyle = (isDragging, draggableStyle) => ({
  // some basic styles to make the items look a bit nicer
  userSelect: "none",
  padding: grid * 2,
  margin: `0 0 ${grid}px 0`,

  // change background colour if dragging
  background: isDragging ? "#6e49f2" : "#f8f8f8",

  // styles we need to apply on draggables
  ...draggableStyle
});

const getListStyle = isDraggingOver => ({
  background: isDraggingOver ? "lightblue" : "#f7f7f7",
  padding: grid,
  width: 250
});

function Task() {
    const [modalState, setModalState] = useState(false);
    const [taskTilte, setTaskTilte] = useState("");
    const [startTime, setStartTime] = useState("");
    const [endtime, setEndtime] = useState("");
    const [items, setItems] = useState([])

    const addNewTask = event =>{
        event.preventDefault();
        setItems([
            ...items,
            {
                id: items.length,
                taskTilte: taskTilte,
                startTime: startTime,
                endtime: endtime
            }
        ]);
        setTaskTilte("");
        setStartTime("");
        setEndtime("");
        setModalState(false)
    }

    const onDragEnd = (result) => {
        // dropped outside the list
        if (!result.destination) {
          return;
        }
        let data = items;
        const getData = reorder(
          data,
          result.source.index,
          result.destination.index
        );
        setItems(getData)
        
    }
    
    
    return (   
        <div>     
            <LayoutWrapper />
            <Header />
            <div className="container">
                <div className="row">
                    <div className="col-lg-12 col-md-12 col-xs-12 col-sm-12 pt-20">
                        <div className="row">
                            <div className="col-md-3"></div>
                            
                            <div className="col-md-9">
                                <table className="table_wdth">                                    
                                    <thead>
                                        <tr>
                                            <th  className="heading_fnt" width="69%">Today</th>
                                            <th className="pt-35">7h 15min</th>
                                        </tr>
                                    </thead>
                                    <DragDropContext onDragEnd={onDragEnd}>
                                        <Droppable droppableId='favouriteDroppable'>
                                            {(provided, snapshot) => (
                                                <tbody {...provided.droppableProps} ref={provided.innerRef} style={getListStyle(snapshot.isDraggingOver)}>
                                                    {items.map((item, index )=> (
                                                        <Draggable key={'item-'+item.id} draggableId={'item-'+item.id} index={index}>
                                                            {(provided, snapshot) => (
                                                                <tr ref={provided.innerRef}
                                                                {...provided.draggableProps}
                                                                {...provided.dragHandleProps}
                                                                style={getItemStyle(
                                                                    snapshot.isDragging,
                                                                    provided.draggableProps.style
                                                                )}>                                            
                                                                    <td className="pdt-10">
                                                                        <i className="fa fa-calendar-o icon_clr"></i>                                                    
                                                                        <span className="title_hding">{item.taskTilte}</span>
                                                                        <span className="time_hding">{item.startTime} - {item.endtime}</span>
                                                                    </td>
                                                                
                                                                    <td className="pdt-10">
                                                                        <span className="time_hding flt_right">1h 30min</span>
                                                                    </td>
                                                                </tr>
                                                            )}                                                        
                                                        </Draggable>
                                                    ))}
                                                    {provided.placeholder}
                                                </tbody>
                                            )}                                            
                                        </Droppable>  
                                    </DragDropContext>                           
                                </table>     
                            </div>                            
                        </div>
                    </div>

                    {/* Tomorrow */}
                    <div className="col-lg-12 col-md-12 col-xs-12 col-sm-12 pt-20">
                        <div className="row">
                            <div className="col-md-3"></div>
                            <div className="col-md-9">
                                <table className="table_wdth">
                                    <thead>
                                        <tr>
                                            <th  className="heading_fnt" width="69%">Tomorrow</th>
                                            <th className="pt-35">6h 30min</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td className="pdt-10">
                                                <i className="fa fa-calendar-o icon_clr"></i> 
                                                <span className="title_hding">Breakfast with the Marking Team</span>
                                                <span className="time_hding">7:00 - 8:30 AM</span>
                                            </td>
                                            <td className="pdt-10">
                                                <span className="time_hding">1h 30min</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td className="pdt-10">
                                                <i className="fa fa-circle icon_clr_circle"></i> 
                                                <span className="title_hding">Process email <i className="fa fa-thumb-tack title_icon_clr"></i> <i className="fa fa-refresh title_icon_clr"></i></span>
                                                <span className="time_hding">8:30 - 9:30 AM</span>
                                            </td>
                                            <td className="pdt-10">
                                                <span className="time_hding">1h 30min</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td className="pdt-10">
                                                <i className="fa fa-calendar-o icon_clr"></i> 
                                                <span className="title_hding">Daily Stand-up meeting</span>
                                                <span className="time_hding">9:30 - 10:00 AM</span>
                                            </td>
                                            <td className="pdt-10">
                                                <span className="time_hding">1h 30min</span>
                                            </td>
                                        </tr>
                                    </tbody>                                  
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="addButton" onClick={() => setModalState(true)}>+</div>
            
            {/* Modal Popup Start */}
            <Modal open={modalState} onClose={() => setModalState(false)} center styles={{ modal:{ minWidth: 500 }}}>
                <div className="modal-body">
					<div className="form-group">
						<label htmlFor="tasktitle">Task Title</label>
						<input type="text" value={taskTilte} className="form-control" name="taskTilte"  onChange={e => setTaskTilte(e.target.value)} />
					</div>
					<div className="form-group">
						<label htmlFor="startTime">Start Time</label>
						<input type="time" className="form-control" value={startTime} name="startTime" onChange={e => setStartTime( e.target.value)}/>
					</div>
					<div className="form-group">
						<label htmlFor="endtime">End Time</label>
						<input type="time" className="form-control" value={endtime} name="endtime" onChange={e => setEndtime(e.target.value)}/>
					</div>
				</div>
				<div className="modal-footer">					
					<button className="btn btn-success" onClick={addNewTask}> Add
					</button>
                    <button className="btn btn-default" onClick={() => setModalState(false)}> Close
					</button>
				</div>
            </Modal>
            {/* Modal Popup End */}
        </div>      
    );
}

export default Task
